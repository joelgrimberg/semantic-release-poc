Epic Changelog

## [1.0.7](https://gitlab.com/joelgrimberg/semantic-release-poc/compare/v1.0.6...v1.0.7) (2022-03-22)


### Bug Fixes

* another one bites the dust 🎵 ([becca57](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/becca57d6a4e1790a67f210c69e6855839c37c69))

## [1.0.6](https://gitlab.com/joelgrimberg/semantic-release-poc/compare/v1.0.5...v1.0.6) (2022-03-22)


### Bug Fixes

* another one bites the dust ([60d5de4](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/60d5de4b1f4bd80c4d789dce835b28d068813dbe))

## [1.0.5](https://gitlab.com/joelgrimberg/semantic-release-poc/compare/v1.0.4...v1.0.5) (2022-03-22)


### Bug Fixes

* some heavy lifting ([c88bb12](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/c88bb12a1afc823caf0f7df62900b00f401b3eaa))

## [1.0.4](https://gitlab.com/joelgrimberg/semantic-release-poc/compare/v1.0.3...v1.0.4) (2022-03-22)


### Bug Fixes

* fix some ([6ff10db](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/6ff10db59e25dd2f2a00abfceff55553153dcf1d))
* fix some ([f54f301](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/f54f30145591ffe9752eb0b237802e77c2cd568b))
* fix some ([0282618](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/0282618c00cb4b83f57fe4b1075e55fa6edd5614))
* fix some ([7032b1b](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/7032b1bb74609372d4f131d98e25825b18f0c95e))
* touch something ([998818b](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/998818b360bf04b5277a8bb2ccbed873fc0bbbb3))
* wut 🧠 ([9dbd92f](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/9dbd92f697e52ee785c3f0aed9b2a7a454832fee))

## [1.0.3](https://gitlab.com/joelgrimberg/semantic-release-poc/compare/v1.0.2...v1.0.3) (2022-03-22)


### Bug Fixes

* some epic fix again ([0d1a39f](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/0d1a39f7f7ac205ea461ecf68bb10b56263e8bb4))

## [1.0.2](https://gitlab.com/joelgrimberg/semantic-release-poc/compare/v1.0.1...v1.0.2) (2022-03-22)


### Bug Fixes

* another fix ([182a127](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/182a1270e1b089ac4ea1720a1bb271368c57bcc5))

## [1.0.1](https://gitlab.com/joelgrimberg/semantic-release-poc/compare/v1.0.0...v1.0.1) (2022-03-22)


### Bug Fixes

* progress ([81254cb](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/81254cb99532581efdab5d89aa31773b4979dd0a))
* some epic fix ([0745e16](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/0745e1673a611bc25e5bea8f75ec1a051f70757b))

# 1.0.0 (2022-03-22)


### Bug Fixes

* fix some stuff ([91d9431](https://gitlab.com/joelgrimberg/semantic-release-poc/commit/91d9431b2ac24d673984dd1ebda9bf3b334c2e63))
